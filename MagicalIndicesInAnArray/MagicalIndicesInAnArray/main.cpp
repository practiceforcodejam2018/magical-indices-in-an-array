#include <iostream>
#include <vector>


using namespace std;

class Engine
{
    private:
        vector<int> populatejIndicesOfVector(int size , vector<int> iIndicesVector)
        {
            vector<int> jIndicesVector;
            for (int i = 1 ; i <= size ; i++)
            {
                int jIndiceComputedValue = ((i + iIndicesVector[i-1]) % size) + 1;
                jIndicesVector.push_back(jIndiceComputedValue);
            }
            return jIndicesVector;
        }
    
    public:
        int getCountOfMagicIdicesInVector(vector<int> iIndicesVector)
        {
            int         size           = (int)iIndicesVector.size();
            vector<int> jIndicesVector = populatejIndicesOfVector(size , iIndicesVector);
            int         count          = 0;
            for(int i = 0 ; i < size ; i++)
            {
                int currentPointingIndex = i;
                for (int j = 0 ; j < size ; j++)
                {
                    if(jIndicesVector[currentPointingIndex] == i + 1)
                    {
                        count++;
                        break;
                    }
                    currentPointingIndex = jIndicesVector[currentPointingIndex] - 1;
                }
            }
            return count;
        }
};


int main(int argc, const char * argv[])
{
    vector<int> iIndicesVector = {2 , 1 , 0 , 4};//{0 , 0 , 0 , 2};
    Engine      e              = Engine();
    cout<<e.getCountOfMagicIdicesInVector(iIndicesVector)<<endl;
    return 0;
}
